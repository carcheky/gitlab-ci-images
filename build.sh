current_folder=$(pwd)
build_folder=$1
cd $build_folder && current_folder_name=$(basename $build_folder) && source .env || exit 1

docker_login() {
    # si existe $DOCKER_REGISTRY_USER, $DOCKER_REGISTRY_PASSWORD y $CI_REGISTRY_IMAGE
    if [ -n "$DOCKER_REGISTRY_USER" ] && [ -n "$DOCKER_REGISTRY_PASSWORD" ] && [ -n "$CI_REGISTRY_IMAGE" ]; then
        docker login -u $DOCKER_REGISTRY_USER -p $DOCKER_REGISTRY_PASSWORD
        docker pull $CI_REGISTRY_IMAGE:latest || true
    fi
}

docker_push() {
    if [ -n "$DOCKER_REGISTRY_USER" ] && [ -n "$DOCKER_REGISTRY_PASSWORD" ] && [ -n "$CI_REGISTRY_IMAGE" ]; then
        docker push carcheky/$current_folder_name --all-tags
    fi
}

make_tags() {
    tags="-t carcheky/$current_folder_name:local"
    # si la rama actual es dev
    if [ "$CI_COMMIT_BRANCH" = "dev" ]; then
        tags="-t carcheky/$current_folder_name:latest \
            -t carcheky/$current_folder_name:$BASE_IMAGE-$BASE_IMAGE_VERSION-$CI_COMMIT_BRANCH"
    # si es main
    elif [ "$CI_COMMIT_BRANCH" = "main" ]; then
        apk add git
        tags="-t carcheky/$current_folder_name:latest \
            -t carcheky/$current_folder_name:$BASE_IMAGE-$BASE_IMAGE_VERSION-latest \
            -t carcheky/$current_folder_name:$BASE_IMAGE-$BASE_IMAGE_VERSION \
            -t carcheky/$current_folder_name:$BASE_IMAGE-$BASE_IMAGE_VERSION-$(git describe --tags --abbrev=0)"
    fi
    echo $tags
}

docker_build() {
    docker build --cache-from carcheky/$current_folder_name --progress=plain --pull --rm --build-arg IMAGE=$BASE_IMAGE --build-arg VERSION=$BASE_IMAGE_VERSION $tags .
}

# logics
docker_login
make_tags
docker_build
docker_push
