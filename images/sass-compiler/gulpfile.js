var gulp = require('gulp');
var sass = require('gulp-sass')(require('sass'));
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var cleanCSS = require('gulp-clean-css');
var cached = require('gulp-cached');
var dependents = require('gulp-dependents');
var debug = require('gulp-debug');
var rename = require('gulp-rename');
var browserSync = require('browser-sync').create();

// BUILD SUBTASKS
// ---------------

// print help
console.log("available subtasks:");
console.log("     styles-dev,");
console.log("     styles-min,");
console.log("     watch-dev,");
console.log("     watch-min,");
console.log("     browsersync-webapp,");
console.log("     browsersync-app,");
console.log("     browsersync-php");

gulp.task('styles-dev', function () {
  return gulp.src('scss/**/*.scss')
    .pipe(cached('sass'))
    .pipe(dependents())
    .pipe(debug({ title: 'scss:' }))
    .pipe(sourcemaps.init())
    .pipe(
      sass({
        includePaths: '/var/www/html/themes/contrib/bootstrap5/scss'
      }).on('error', sass.logError)
    )
    .pipe(autoprefixer())
    .pipe(rename({ dirname: '' }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('css'))
    .pipe(debug({ title: 'css:' }))
});

// styles-min:
gulp.task('styles-min', function () {
  return gulp.src('scss/**/*.scss')
    .pipe(cached('sass'))
    .pipe(dependents())
    .pipe(debug({ title: 'scss:' }))
    .pipe(sass())
    .pipe(autoprefixer())
    .pipe(cleanCSS())
    .pipe(rename({ dirname: '' }))
    .pipe(gulp.dest('css'))
    .pipe(debug({ title: 'css:' }))
});

// BUILD TASKS
// ------------

gulp.task('watch-dev', function () {
  gulp.watch(
    "scss/**/*.scss", {
    events: 'all',
    ignoreInitial: false
  }, gulp.series('styles-dev'));
});

gulp.task('watch-min', function () {
  gulp.watch(
    "scss/**/*.scss", {
    events: 'all',
    ignoreInitial: false
  }, gulp.series('styles-min'));
});

gulp.task('browsersync-webapp', function () {
  browserSync.init({
    proxy: "http://webapp"
  });
  gulp.watch(
    "scss/**/*.scss", {
    events: 'all',
    ignoreInitial: false
  }, gulp.series('styles-dev'));
  gulp.watch("css/*.css",).on('change', browserSync.reload);
  gulp.watch("js/*.js",).on('change', browserSync.reload);
  gulp.watch("templates/**/*.twig",).on('change', browserSync.reload);
});

gulp.task('browsersync-php', function () {
  browserSync.init({
    proxy: "http://php"
  });
  gulp.watch(
    "scss/**/*.scss", {
    events: 'all',
    ignoreInitial: false
  }, gulp.series('styles-dev'));
  gulp.watch("css/*.css",).on('change', browserSync.reload);
  gulp.watch("js/*.js",).on('change', browserSync.reload);
  gulp.watch("templates/**/*.twig",).on('change', browserSync.reload);
});

gulp.task('browsersync-app', function () {
  browserSync.init({
    proxy: "http://app"
  });
  gulp.watch(
    "scss/**/*.scss", {
    events: 'all',
    ignoreInitial: false
  }, gulp.series('styles-dev'));
  gulp.watch("css/*.css",).on('change', browserSync.reload);
  gulp.watch("js/*.js",).on('change', browserSync.reload);
  gulp.watch("templates/**/*.twig",).on('change', browserSync.reload);
});

